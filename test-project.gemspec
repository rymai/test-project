# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'test-project/version'

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 2.6'
  spec.name          = 'test-project'
  spec.version       = TestProject::VERSION
  spec.authors       = ['Rémy Coutable']
  spec.metadata['allowed_push_host'] = 'https://gems.my-company.example'

  spec.summary       = 'Test project to test gem release.'
  spec.homepage      = 'https://gitlab.com/rymai/test-project'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(docs|test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
