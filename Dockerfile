FROM python:3.6-alpine

RUN apk --no-cache add \
    build-base \
    libffi-dev \
    openssl-dev \
    libxslt-dev \
    db-dev \
    bash

WORKDIR /app

CMD [ "./scrapyrunner.py" ]
